let collection = [];


// Write the queue functions below.
	

// 1. Output all the elements of the queue
	function print(){
		return collection
	}


// 2. Adds element to the rear of the queue
		
	function enqueue(Anthea){
		this.collection.push(Anthea)
		return collection
	}


// 3. Removes element from the front of the queue
	 function dequeue(Elaine){
	 	this.collection.shift(Elaine);
	 	return collection
	 }


// 4. Show element at the front
	function front(){
		return this.collection[0]
	}


// 5. Show the total number of elements
		function size(){
			return collection.length;
		}
	

// 6. Outputs a Boolean value describing whether queue is empty or not
	function isEmpty(){
		return this.collection.length === 0
	}



module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};
